﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace MvcCoreMovie.Models
{
    public class MvcCoreMovieContext : DbContext
    {
        public MvcCoreMovieContext(DbContextOptions<MvcCoreMovieContext> options)
            : base(options)
        {
        }

        public DbSet<MvcCoreMovie.Models.Movie> Movie { get; set; }
    }
}
